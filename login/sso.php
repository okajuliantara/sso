<?php
session_start();

include "../JWT/JWT.php";

$ssoServerLink = 'https://sso.baliprov.go.id/authBroker/';

$redirect = 'http://localhost/sso/sso';
$urlToRedirect = 'http://localhost/sso/sso';
$logoutLink = 'http://localhost/sso/logout';

//jika terdapat kiriman data dari server SSO
if(!empty($_GET['token'])){
	echo "Token : ".$_GET['token'].'<br />'.'<br />';

	$JWT = new JWT();
	$JWT->setJWTString($_GET['token']);

	if($JWT->decodeJWT()){
		// if session is valid then set session and redirect page
		if(session_id()==$JWT->getPayloadJWT()->sessionRequest){					
			$_SESSION['UserIsAuthenticated'] = 1;
			$_SESSION['authUserData'] = $JWT->getPayloadJWT();

			echo "Session valid ! <br />";
			echo "urlToRedirect : ".$_SESSION['authUserData']->urlToRedirect."<br />";
			echo "ssoLogoutLink : ".$_SESSION['authUserData']->ssoLogoutLink."<br />";
			echo "name : ".$_SESSION['authUserData']->user->name."<br />";
			echo "username : ".$_SESSION['authUserData']->user->username."<br />";
			echo "role id : ".$_SESSION['authUserData']->roles[0]->id."<br />";
			echo "role code : ".$_SESSION['authUserData']->roles[0]->role_code."<br />";
			echo "role name : ".$_SESSION['authUserData']->roles[0]->name."<br />";
			echo "<br />";
			echo "session id : ".session_id()."<br />";
			echo "<br />";
			echo var_dump($_SESSION['authUserData']);
			exit;
		}else{
			//if session is invalid
			echo("Invalid browser session !");exit;
		}
	}else{
		echo("Invalid JWT data !");exit;
	}
}

// user already authenticated
if(!empty($_SESSION['authUserData'])){
	return 1;
}else{
	// if a user not yet authenticated, then redirect to SSO server
	$payloadJWT = [
		'redirect' 			=> $redirect,
		'urlToRedirect'		=> $urlToRedirect,
		'logoutLink' 		=> $logoutLink,
		'kode_broker'		=> '086165',
		'sessionRequest' 	=> session_id()
	];
	$JWT = new JWT();
	$JWT->setPayloadJWT($payloadJWT);
	$JWT->encodeJWT();
	header('Location:'.$ssoServerLink.$JWT->getJWTString());exit;
}
?>